﻿using System;

namespace LV1_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            //3. zadatak
            Note note;
            note = new Note();
            Console.WriteLine( note.getOwner());
            Console.WriteLine("Note: " + note.getText());
            note.setText("Kupi kruh");
            Console.WriteLine( note.getOwner());
            Console.WriteLine("Note: " + note.getText());

            note = new Note("Ocisti snijeg", 1, "Filip");
            Console.WriteLine(note.getOwner());
            Console.WriteLine("Note: " + note.getText());

            note = new Note("Poslusaj predavanje danas u 16:45");
            Console.WriteLine(note.getOwner());
            Console.WriteLine("Note: " + note.getText());

            //4
            Console.Write("unesi biljesku: ");
            note.Text = Console.ReadLine(); 
            Console.WriteLine();

            Console.WriteLine(note.Owner);
            Console.WriteLine("Note: " + note.Text);

            //5
            Console.WriteLine(note.ToString());

            //6
            Console.WriteLine("TIMENOTE");
            note = new Timenote();
            Console.WriteLine(note.ToString());
        }
    }
}
