﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1_RPPOON
{
    class Note
    {
        private String text;
        private int priority;
        private String owner;
        #region getteri i setteri
        public string getText() { return this.text; }
        public int getLevel() { return this.priority; }
        public string getOwner() { return this.owner; }
        public void setText(string text) { this.text = text; }
        public void setLevel(int lvl) { this.priority = lvl; }
        private void setOwner(string owner) { this.owner = owner; }
        #endregion

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public int Priority
        {
            get { return this.priority; }
            set { this.priority = value; }
        }
        public string Owner
        {
            get { return this.owner;  }
            private set { this.owner = value; }
        }


        #region Ctors
        public Note()
        {
            text = "";
            priority = 0;
            owner = "Root";
        }
        public Note(string text)
        {
            this.text = text;
            this.priority = 0;
            this.owner="Root";
        }

        public Note(string text, int priority, string owner)
        {
            this.text = text;
            this.priority = priority;
            this.owner=owner;
        }
        #endregion

        public override string ToString()
        {
            return this.Owner + ": " + this.Text;
        }
    }
}
